# consultUs #

![consultUS](images/cs1.png)

### Introduction ###

Often a times we see our consultations are limited to our city or town . And with the current pandemic going on , it is risky to go hospital or clinic for consultation. With virtual clinic , we tend to remove this limitation . Using ConsultUs, you can connect with specialists all over the world  with a single click . 

### How can a Doctor Register ? ###

* Click on the Register button in the navbar
* Fill the personal details
* Fill the avaialable days and time for consultancy 
* Once form is filled you will be redirected to dashboard 
* All the appointments will be shown on appointment section of dashboard

![Doctor Registration Form](images/cs2.png)

### How can patient book appointment? ###

* Select the Deparmtment 
* Click on see doctor to check all available doctors
* On selecting the doctor Click on book appointment
* Fill the form and select the slot 
* You will be redirected to Connect us page where you can connect with the doctor directly

![Patient Appointment Booking Form](images/cs3.png)

### Stack ###
* React
* Firebase
* Jitsi

### Copyright ###
The following work was done during the hackathon HackJaipur by team Dev.ino. The team Dev.ino reserves the right to each part and section of this code and this should not be reproduced without their consent and permission.