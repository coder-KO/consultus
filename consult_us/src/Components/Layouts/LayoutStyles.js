import { makeStyles } from '@material-ui/core/styles';

const LayoutStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    icon: {
      marginRight: theme.spacing(2),
      color: 'white',
      fontSize: 40,
    },
    iconScroll: {
      marginRight: theme.spacing(2),
      color: 'black',
      fontSize: 40,
    },
    heroContent: {
      backgroundColor: theme.palette.background.paper,
      backgroundImage: '../../assets/home-bg.jpg',
      padding: theme.spacing(0, 0, 6),
    },
    heroButtons: {
      marginTop: theme.spacing(4),
    },
    footer: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(6),
    },
    show: {
      color: 'red',
    },
    hide:{
      color: 'white',
    },
    title: {
      flexGrow: 1,
    },
  }));

  export default LayoutStyles;