import React from "react";
import AppBar from "@material-ui/core/AppBar";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

import LayoutStyles from "./LayoutStyles";

export default function Navbar() {
  const classes = LayoutStyles();
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  return (
    <div className={classes.root}>
      <AppBar
        elevation={trigger ? 3 : 0}
        position="fixed"
        color={trigger ? "white" : "transparent"}
        // style={{ color: trigger ? "black" : "white" }}
      >
        <Toolbar>
          <Link to={`/`} style={{ color: "inherit", textDecoration: "none" }}>
            <LocalHospitalIcon
              className={trigger ? classes.iconScroll : classes.icon}
            />
          </Link>

          <Typography
            variant="h6"
            noWrap
            className={classes.title}
            style={{ fontFamily: "Kaushan Script" }}
          >
            <Link to={`/`} style={{ color: "inherit", textDecoration: "none" }}>
              consultUS
            </Link>
          </Typography>

          <Button color="inherit" component={Link} to={`/doctor/login/form`}>
            Login
          </Button>
          <Button color="inherit" component={Link} to={`/doctor/form`}>
            Register
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}
