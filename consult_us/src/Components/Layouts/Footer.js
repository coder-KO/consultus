import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

import LayoutStyles from './LayoutStyles'

function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://material-ui.com/">
          Dev.ino
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
}

export default function CenteredGrid() {
  const classes = LayoutStyles();

  return (
    <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          consultUS
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Your Virtual Clinic!
        </Typography>
        <Copyright />
      </footer>
  );
}