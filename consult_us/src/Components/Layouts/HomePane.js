import React from "react";
import { Parallax, Background } from "react-parallax";
import Paper from '@material-ui/core/Paper';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

import LayoutStyles from "./LayoutStyles";

export default function HomePane() {
  const classes = LayoutStyles();

  return (
    <div className={classes.heroContent}>
      <Parallax
        blur={7}
        bgImage={require("../../assets/imgs/home-bg.jpg")}
        bgImageAlt="Background Image"
        strength={500}
      >
        <Container maxWidth="sm" style={{ marginTop: "8em", height: "20vh" }}>
            <Typography
              component="h1"
              variant="h2"
              align="center"
              gutterBottom
              style={{color: '#dddddd', fontFamily: 'Josefin Sans',}}
              elevation={3}
            >
              Your Virtual Clinic
            </Typography>
            <Typography
              variant="h5"
              align="center"
              paragraph
              style={{color: 'white', fontFamily: 'Kaushan Script',}}
            >
              Now your consultancy is not limited to your city.. Consult with doctors all around the world with 
              simple procedures and get Quicker prescription.<br/>
              We value your health!
            </Typography>
        </Container>
        <div style={{ height: "200px" }} />
      </Parallax>
      <Container maxWidth="xs" style={{textAlign: 'center', marginTop: '5rem', marginBottom: '0', fontFamily: 'Josefin Sans', fontSize: 30,}}>Departments</Container>
    </div>
  );
}
