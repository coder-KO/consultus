import React from "react";
import { Navbar, Footer, HomePane } from "../Layouts";
import CssBaseline from "@material-ui/core/CssBaseline";
import DepartmentCards from "../../Components/Departments/DepartmentCards";


export default function HomePage() {
  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Navbar />
        <main>
          <HomePane />
          <DepartmentCards />
        </main>
        <Footer />
      </React.Fragment>
    </>
  );
}
