import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../../containers/GeneralCss/UseStyles";
import firebase from "firebase";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { PatId, DoctorId } from "../../containers/getIdContainer/getId";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Tooltip from "@material-ui/core/Tooltip";
import InfoIcon from "@material-ui/icons/Info";
import PayPalButton from "../Paypal/PayPalButton";
import BookAppointment from "./BookAppointment"
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from "@material-ui/icons/Check";

const patientDb = firebase.database().ref("Patients");
const doctorDb = firebase.database().ref("Doctors");
const storage = firebase.storage().ref();

export default function PatientForm({ match, history }) {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  const [paramState, setParamState] = React.useState({
    Department: match.params.department,
    Doc_id: match.params.docid,
    Doc_name: match.params.name,
  });

  const [state, setState] = React.useState({
    Name: "",
    Age: "",
    Gender: "",
    Issue: "",
    PhNo: "",
    Email: "",
    Dept: "",
    AppointmentID: "",
  });

  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  }

  const [state1, setState1] = React.useState({
    image: "",
    Url: "",
  });

  function GetImage(e) {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      state1.image = image;
    }
    state1.flag = 1;
    console.log(e.target.files[0], state1.flag);
  }

  // uploading image to storage and fetching url

  function UploadTask(e) {
    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const image = state1.image;
      const upload = storage
        .child("Patients")
        .child(PatId + `/${image.name}`)
        .put(image);
      console.log("this is upload_task", upload);
      upload.on(
        "state_changed",
        (snapshot) => {
          // progress function ...
        },
        (error) => {
          // Error function ...
          console.log(error);
        },
        () => {
          // complete function ...

          

          storage
            .child("Patients")
            .child(PatId + "/" + image.name)
            .getDownloadURL()
            .then((url) => {
              state1.Url = url;
              console.log(url);
              setSuccess(true);
              setLoading(false);
            });
        }
      );
    }
  }

  function handleSubmit(event) {
    console.log("in submission");

    event.preventDefault();

    var data = {
      Name: state.Name,
      Issue: state.Issue,
      Age: state.Age,
      Phone_No: state.PhNo,
      Email: state.Email,
      Url: state1.Url,
      Gender: state.Gender,
      Department: paramState.Department,
      docid: paramState.Doc_id,
    };
    console.log(data, PatId);

    patientDb.child(PatId).set(data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("data pushed");
      }
    });

    var id = PatId + 1;

    patientDb
      .child("ID")
      .set(id, (err) => {
        if (err) console.log(err);
        else {
          console.log("set id");
        }
      })
      // .then(setAppointment())
      .then(
        history.push(
          "/patient/appointment/link/" +
            `${id - 1}` +
            "/" +
            `${paramState.Doc_id}`
        )
      );
  }

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.purple}>
          <AssignmentIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Patient Form
        </Typography>
        <ValidatorForm className={classes.form} onSubmit={handleSubmit}>
          <TextValidator
            margin="normal"
            fullWidth
            id="dName"
            label="Doctor's Name"
            control="input"
            name="Dname"
            value={paramState.Doc_name}
            // onChange={handleChange}
            autoFocus
            InputProps={{
              readOnly: true,
            }}
          />

          <TextValidator
            margin="normal"
            fullWidth
            id="Name"
            label="Name"
            control="input"
            name="Name"
            value={state.Name}
            onChange={handleChange}
            autoFocus
            validators={["required"]}
            errorMessages={["this field is required"]}
          />

          <TextValidator
            margin="normal"
            fullWidth
            type="number"
            id="Age"
            label="Age"
            control="input"
            name="Age"
            value={state.Age}
            validators={["required", "matchRegexp:[0-9]+$"]}
            errorMessages={["this field is required", "Please enter a number"]}
            onChange={handleChange}
            autoFocus
          />

          <TextValidator
            margin="normal"
            fullWidth
            name="Issue"
            label="Issue"
            onChange={handleChange}
            id="Issue"
            value={state.Issue}
            validators={["required"]}
            errorMessages={["this field is required"]}
          />

          <FormControl
            component="fieldset"
            margin="normal"
            fullWidth
            className={classes.radio_purple}
          >
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup
              aria-label="gender"
              name="Gender"
              value={state.Gender}
              required
            >
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
                onClick={handleChange}
              />
              <FormControlLabel
                value="male"
                control={<Radio />}
                label="Male"
                onClick={handleChange}
              />
              <FormControlLabel
                value="other"
                control={<Radio />}
                label="Other"
                onClick={handleChange}
              />
            </RadioGroup>
          </FormControl>

          {/* validation field for email  */}

          <TextValidator
            margin="normal"
            fullWidth
            label="Email"
            onChange={handleChange}
            name="Email"
            value={state.Email}
            validators={["required", "isEmail"]}
            errorMessages={["this field is required", "email is not valid"]}
          />

          {/* validation field for phone number */}

          <TextValidator
            margin="normal"
            fullWidth
            name="PhNo"
            label="PhoneNumber"
            id="PhoneNumber"
            validators={["required", "matchRegexp:^[0-9]{10}$"]}
            value={state.PhNo}
            errorMessages={["this field is required", "phone no is not valid"]}
            onChange={handleChange}
          />

          {/* Select department */}

          <TextValidator
            margin="normal"
            fullWidth
            name="Dept"
            label="Department"
            id="Department"
            value={paramState.Department}
            InputProps={{
              readOnly: true,
            }}
          />

          <br />
          <br />

          {/* uploading image  */}
          <FormLabel>
            <span>Upload Documents</span>
            <Tooltip title="Upload your test reports here ">
              <InfoIcon />
            </Tooltip>
          </FormLabel>

          <div className={classes.root}>
            <input
              accept="image/*"
              className={classes.input}
              id="icon-button-file"
              type="file"
              onChange={GetImage}
            />

            <label htmlFor="icon-button-file">
              <IconButton
                color="primary"
                aria-label="upload picture"
                component="span"
              >
                <PhotoCamera />
              </IconButton>
            </label>
            <label htmlFor="contained-button-file">
              <Button
                variant="contained"
                color="primary"
                component="span"
                onClick={UploadTask}
              >
                Upload
              </Button>
            </label>
            {/* <label>{loading && <CircularProgress />}</label>
            <label>{success && <CheckIcon />}</label> */}
          </div>

          <br />
          <br />

          <BookAppointment doc_id={paramState.Doc_id} />  

          {/* Checkbox */}
          <FormControlLabel
            control={
              <Checkbox value="remember" color="primary" required="true" />
            }
            label="I agree all the information given by me are correct"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            Submit{" "}
          </Button>
        </ValidatorForm>
      </div>

      <PayPalButton />
    </Container>
  );
}
