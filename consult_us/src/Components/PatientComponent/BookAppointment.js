import React, { useState } from "react";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import Grid from "@material-ui/core/Grid";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import firebase from "firebase";
import { Lines } from "react-preloaders";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",

    color: theme.palette.text.secondary,
  },
}));

const data = firebaseDB.database().ref("Doctors");
const currentDate = new Date();
let dateSelected = currentDate.getDate();
let daySelected = currentDate.getDay();

export default function BookAppointment(props) {
  const classes = useStyles();
  console.log(props)

  const [date, changeDate] = useState(currentDate);
  const [days, setDays] = useState([]);
  const [visibility, setVisibility] = useState(false);
  const [loading, setLoading] = useState(false);
  const [slot, setSlot] = useState([]);

  // console.log(date.getMonth());
  // var d = date.getDate() + 6;
  // const Day_arr = [];

  React.useEffect(() => {
    setLoading(true);
    data
      .child(props.doc_id)
      .child("Schedule")
      .once("value")
      .then((snapshot) => {
        const Day_arr = [];
        snapshot.forEach((childSnapshot) => {
          const values = childSnapshot.key;
          console.log(values);
          if (values === "Monday") {
            Day_arr.push(1);
          }

          if (values === "Tuesday") {
            Day_arr.push(2);
          }

          if (values === "Wednesday") {
            Day_arr.push(3);
          }

          if (values === "Thursday") {
            Day_arr.push(4);
          }

          if (values === "Friday") {
            Day_arr.push(5);
          }

          if (values === "Saturday") {
            Day_arr.push(6);
          }

          if (values === "Sunday") {
            Day_arr.push(0);
          }

          // console.log(values);
        });
        updateState(Day_arr);
      });
  }, []);

  function updateState(Day_arr) {
    setDays.days = Day_arr;
    setLoading(false);
  }

  function callDate(date) {
    changeDate(date);
    if (date.getDay() === 0) {
      var day = "Sunday";
    }
    if (date.getDay() === 1) {
      day = "Monday";
    }
    if (date.getDay() === 2) {
      day = "Tuesday";
    }
    if (date.getDay() === 3) {
      day = "Wednesday";
    }
    if (date.getDay() === 4) {
      day = "Thursday";
    }
    if (date.getDay() === 5) {
      day = "Friday";
    }
    if (date.getDay() === 6) {
      day = "Saturday";
    }

    data
      .child(props.doc_id)
      .child("Schedule")
      .child(day)
      .once("value")
      .then((snapshot) => {
        console.log(snapshot.val());
        var app = {
          start_time: snapshot.val().start_time,
          end_time: snapshot.val().end_time,
          // slots: snapshot.val().Slot,
        };
        var slots_arr = [];
        for (let [key, value] of Object.entries(snapshot.val().Slot)) {
          console.log(`${key}: ${value}`);
          if (value === -1) {
            slots_arr.push(key);
          }
        }
        console.log(slots_arr);
        setSlot(slots_arr);
      });
    setVisibility(true);
    dateSelected = date.getDate();
    daySelected = date.getDay();
    // console.log(dateSelected + ", " + daySelected);
  }

  function disableDays(date) {
    if (setDays.days === undefined) {
      return null;
    } else {
      return !setDays.days.includes(date.getDay());
    }
  }

  return (
    <>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                autoOk
                disablePast={true}
                shouldDisableDate={disableDays}
                orientation="landscape"
                variant="static"
                openTo="date"
                value={date}
                onChange={callDate}
                maxDate={new Date(2020, 5, currentDate.getDate() + 6)}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item xs={3}>
            <Paper className={classes.paper}>Slots</Paper>

            <br />
            {visibility ? (
              <Paper className={classes.paper}>
                <List style={{ maxHeight: "200px", overflow: "auto" }}>
                  <ButtonGroup
                    orientation="vertical"
                    color="primary"
                    aria-label="vertical contained primary button group"
                    variant="text"
                  >
                    {slot.map((value, index) => (
                      <Button>{value}</Button>
                    ))}

                    {/* <Button>15:20</Button>
                    <Button>15:40</Button> */}
                  </ButtonGroup>
                </List>
              </Paper>
            ) : (
              <Paper className={classes.paper}>
                Select a date to get available slots for that day
              </Paper>
            )}
          </Grid>
        </Grid>
      </div>
      <Lines customLoading={loading} />
    </>
  );
}
