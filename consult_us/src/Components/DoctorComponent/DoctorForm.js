import React from "react";
import Navbar from "../Layouts/NavbarCommon";
import ImageUpload from "../Layouts/ImageUpload";
import Scheduler from "./Scheduler";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../../containers/GeneralCss/UseStyles";
import { DoctorId } from "../../containers/getIdContainer/getId";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from '@material-ui/icons/Check';
// importing material ui icons
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import AssignmentIcon from "@material-ui/icons/Assignment";

import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Footer } from "../Layouts";

// importing firebase functionalities
import firebase from "firebase";
import firebaseDB from "../../containers/AuthContainer/vh_auth";

const DoctorDb = firebase.database().ref("Doctors");
const storage = firebase.storage().ref();

export default function DoctorForm(props) {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [state, setState] = React.useState({
    Name: "",
    spec1: "",
    spec2: "",
    Email: "",
    Dept: "",
    Bio: "",
    password: "",
    confirm_password: "",
    error: "",
    error_code: false,
  });

  const [state1, setState1] = React.useState({
    image: "",
    Url: "",
    flag: false,
    send_data: "",
  });

  function GetImage(e) {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      state1.image = image;
    }
    console.log(e.target.files[0], state1.flag);
  }

  // uploading image to storage and fetching url
  ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
    if (value !== state.password) {
      return false;
    }
    return true;
  });

  function UploadTask(e) {
    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const image = state1.image;
      const upload = storage
        .child("Doctors")
        .child(DoctorId + `/${DoctorId}`)
        .put(image);
      console.log("this is upload_task", upload);
      upload.on(
        "state_changed",
        (snapshot) => {},
        (error) => {
          // Error function ...
          console.log(error);
        },
        () => {
          // complete function ...

          storage
            .child("Doctors")
            .child(DoctorId + "/" + DoctorId)
            .getDownloadURL()
            .then((url) => {
              state1.Url = url;
              setSuccess(true);
              setLoading(false);
              console.log(url);
            });
        }
      );
    }
  }

  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  }

  // Form Submission

  function handleSubmit(event) {
    console.log("in submission");
    event.preventDefault();
    console.log(state, state1);

    var data = {
      Name: state.Name,
      Email: state.Email,
      Url: state1.Url,
      Specialization: {
        spec1: state.spec1,
        spec2: state.spec2,
      },
      Department: state.Dept,
      Bio: state.Bio,
      Verification_status: 0,
      password: state.password,
    };

    setState1({ send_data: data, flag: true });
  }

  return (
    <>
      {/* <Navbar /> */}
      <Navbar />
      <Container component="main" maxWidth="md">
        <CssBaseline />
        {state.error_code ? (
          <Typography>{state.error}</Typography>
        ) : (
          <Typography display="none"></Typography>
        )}

        <div className={classes.paper}>
          <ValidatorForm className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={3} justify="space-around">
              {/* ========== Top of the form ========== */}
              <Grid item xs={12} justify="center" alignItems="center">
                <Avatar className={classes.purple} style={{ margin: "auto" }}>
                  <AssignmentIcon />
                </Avatar>
                <Typography
                  component="h1"
                  variant="h5"
                  style={{ textAlign: "center" }}
                >
                  Doctor's Registration Form{" "}
                </Typography>
              </Grid>

              {/* ========== Left Pane ========== */}
              <Grid item xs={5} style={{ paddingRight: "4rem" }}>
                <TextValidator
                  margin="normal"
                  fullWidth
                  id="Name"
                  label=" Full Name"
                  control="input"
                  name="Name"
                  value={state.Name}
                  onChange={handleChange}
                  autoFocus
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />

                {/* validation field for email  */}

                <TextValidator
                  margin="normal"
                  fullWidth
                  label="Email"
                  onChange={handleChange}
                  name="Email"
                  value={state.Email}
                  validators={["required", "isEmail"]}
                  errorMessages={[
                    "this field is required",
                    "email is not valid",
                  ]}
                />

                {/* set password  */}

                <TextValidator
                  margin="normal"
                  fullWidth
                  type="password"
                  label="Set password"
                  onChange={handleChange}
                  name="password"
                  value={state.password}
                  minlength="6"
                  validators={["required", "minStringLength:12"]}
                  errorMessages={[
                    "set password here",
                    "password must be atleast 12 characters long",
                  ]}
                />

                {/* confirm password field */}

                <TextValidator
                  margin="normal"
                  fullWidth
                  type="password"
                  label="Confirm password"
                  onChange={handleChange}
                  name="confirm_password"
                  value={state.confirm_password}
                  validators={["required", "isPasswordMatch"]}
                  errorMessages={[
                    "Please confirm your password",
                    "Passwords do not match",
                  ]}
                />

                {/* Select department */}

                <FormControl
                  className={classes.formControl}
                  fullWidth
                  margin="normal"
                >
                  <InputLabel id="demo-controlled-open-select-label">
                    Department
                  </InputLabel>
                  <Select
                    labelId="demo-controlled-open-select-label"
                    id="demo-controlled-open-select"
                    value={state.Dept}
                    onChange={handleChange}
                    name="Dept"
                  >
                    <MenuItem value="General" fullWidth>
                      General
                    </MenuItem>
                    <MenuItem value="Neurology" fullWidth>
                      Neurology
                    </MenuItem>
                    <MenuItem value="ENT" fullWidth>
                      ENT
                    </MenuItem>
                  </Select>
                </FormControl>

                <TextValidator
                  margin="normal"
                  fullWidth
                  label="Experience and Bio"
                  onChange={handleChange}
                  name="Bio"
                  value={state.Bio}
                />

                <TextValidator
                  margin="normal"
                  fullWidth
                  label="Specialisation 1"
                  onChange={handleChange}
                  name="spec1"
                  value={state.spec1}
                />

                <TextValidator
                  margin="normal"
                  fullWidth
                  label="Specialisation 2"
                  onChange={handleChange}
                  name="spec2"
                  value={state.spec2}
                />

                <br />
                <br />

                {/* uploading image  */}

                <FormLabel>Upload Profile Picture</FormLabel>

                <div className={classes.root}>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="icon-button-file"
                    type="file"
                    onChange={GetImage}
                    label={state1.image}
                  />

                  <label htmlFor="icon-button-file">
                    <IconButton
                      color="primary"
                      aria-label="upload picture"
                      component="span"
                    >
                      <PhotoCamera />
                    </IconButton>
                  </label>
                  <label htmlFor="contained-button-file">
                    <Button
                      variant="contained"
                      color="primary"
                      component="span"
                      onClick={UploadTask}
                    >
                      Upload
                    </Button>
                  </label>
                  <label>{loading && <CircularProgress />}</label>
                  <label>{success && <CheckIcon />}</label>

                </div>
              </Grid>

              {/* ========== Right Pane ========== */}
              <Grid item xs={6} style={{ paddingLeft: "4rem" }}>
                <Scheduler
                  flag={state1.flag}
                  data={state1.send_data}
                  history={props.history}
                />
              </Grid>

              {/* ========== Bottom Pane ========== */}
              <Grid item xs={12}>
                <Grid item xs={2}></Grid>
                <Grid
                  item
                  xs={6}
                  style={{ textAlign: "center", margin: "auto" }}
                >
                  <br />
                  <br />
                  {/* Checkbox */}
                  <FormControlLabel
                    control={
                      <Checkbox
                        value="remember"
                        color="primary"
                        required="true"
                      />
                    }
                    label="I agree all the information given by me are correct"
                  />

                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={loading}
                  >
                    Submit{" "}
                  </Button>
                </Grid>
                <Grid item xs={2}></Grid>
              </Grid>
            </Grid>
          </ValidatorForm>
        </div>
        <Footer />
      </Container>
    </>
  );
}
