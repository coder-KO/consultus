import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../../containers/GeneralCss/UseStyles";
import firebase from "firebase";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { Footer } from "../Layouts";
import Navbar from "../Layouts/NavbarCommon";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

const DoctorDb = firebase.database().ref("Doctors");

export default function DoctorLoginForm(props) {
  //   console.log(props.match.params.docid);

  const [open, setOpen] = React.useState(true);
  const classes = useStyles();
  const [state, setState] = React.useState({
    id: "",
    Email: "",
    password: "",
    error: "",
  });

  // calling the function to load data...

  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  }



  
  // Form Submission

  function handleSubmit(event) {
    console.log("in submission");

    event.preventDefault();

    firebase
      .auth()
      .signInWithEmailAndPassword(state.Email, state.password)
      .then((user) => {
        // console.log(user);
        DoctorDb.once("value")
          .then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
              const values = childSnapshot.val();
              if (values.Email === state.Email) {
                props.history.push(
                  // eslint-disable-next-line no-useless-concat
                  "/doctor/dashboard/" + `${childSnapshot.key}`
                );
              }
            });
          })
          .catch((error) => {
            console.log(error);

            setState({
              error: "Invalid Email or password",
              Email: "",
              password: "",
            });
            console.log("here");
          });
      })
      .catch((error) => {
        console.log(error);
        setState({
          error: "Invalid Email or password",
          Email: "",
          password: "",
        });
      });
  }

  return (
    <>
      {/* <Navbar /> */}

      {state.error && (
        <Snackbar
          open={true}
          autoHideDuration={6000}
          // onClose={setOpen(false)}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <Alert severity="error">{state.error}</Alert>
        </Snackbar>
      )}
      <Container component="main" maxWidth="xs" style={{ marginTop: "9%" }}>
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.purple}>
            <AssignmentIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Doctor Login Form{" "}
          </Typography>
          <ValidatorForm className={classes.form} onSubmit={handleSubmit}>
            {/* validation field for email  */}

            <TextValidator
              margin="normal"
              fullWidth
              label="Email"
              onChange={handleChange}
              name="Email"
              value={state.Email}
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
            />

            {/* Enter password  */}

            <TextValidator
              margin="normal"
              fullWidth
              type="password"
              label="Enter password"
              onChange={handleChange}
              name="password"
              value={state.password}
              minlength="6"
              validators={["required", "minStringLength:12"]}
              errorMessages={[
                "Enter password here",
                "password must be atleast 12 characters long",
              ]}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              // onClick={handleSubmit}
            >
              Submit{" "}
            </Button>
            <Link
              to={{
                pathname: `/forgot/password/`,
              }}
              style={{ textDecoration: "none" }}
            >
              <Button variant="contained" color="primary">
                Forgot password..
              </Button>
            </Link>
          </ValidatorForm>
        </div>
        <Footer />
      </Container>
    </>
  );
}
