/* eslint-disable no-useless-concat */
import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

import Navbar from "../Layouts/NavbarCommon";
// import DepartmentCardsStyles from '../Departments/DepartmentCardsStyles';
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Drawer from "@material-ui/core/Drawer";
import Avatar from "@material-ui/core/Avatar";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import Paper from "@material-ui/core/Paper";

import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { Lines } from "react-preloaders";
import firebase from "firebase";
import Jitsi from "react-jitsi";
import FormLabel from "@material-ui/core/FormLabel";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from "@material-ui/icons/Check";

const drawerWidth = 240;

const DepartmentCardsStyles = (theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  large: {
    width: theme.spacing(18),
    height: theme.spacing(18),
  },
  // -------------------
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },

  input: {
    display: "none",
  },
});

const data = firebaseDB.database().ref("Doctors");
const storage = firebaseDB.storage().ref();

export class DoctorDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      doc_data: {},
      loading: true,
      image: "",
      Url: "",
      loading_url:false,
      success: false,
    };
  }
  history = this.props;

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.
        console.log(user.email);
        this.setState({ id: this.props.match.params.docid });
        console.log(this.state.id);
        data
          .child(`${this.state.id}`)
          .once("value")
          .then((snapshot) => {
            const data_rec = snapshot.val();
            console.log(data_rec);
            if (data_rec.Email !== user.email) {
              this.props.history.push("/doctor/login/form");
            }

            const received = {
              Name: data_rec.Name,
              Email: data_rec.Email,
              spec1: data_rec.Specialization.spec1,
              spec2: data_rec.Specialization.spec2,
              Url: data_rec.Url,
              Bio: data_rec.Bio,
              verify: data_rec.Verification_status,
            };

            this.setState({
              doc_data: received,
              loading: false,
            });
          });
      } else {
        // No user is signed in.
        this.props.history.push("/doctor/login/form");
      }
    });
  }

  GetImage(e) {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState({ image: image });
    }
  }

  UploadTask(e) {
    if (!this.state.loading_url) {
      this.setState({ success: false, loading_url: true });
      const image = this.state.image;
      console.log(this.state)
      const upload = storage
        .child("Doctors")
        .child(this.state.id + `/${this.state.id}` + "_document")
        .put(image);
      console.log("this is upload_task", upload);
      upload.on(
        "state_changed",
        (snapshot) => {},
        (error) => {
          // Error function ...
          console.log(error);
        },
        () => {
          // complete function ...\
          // var final = this.state.doc_data;

          storage
            .child("Doctors")
            .child(this.state.id + `/${this.state.id}` + "_document")
            .getDownloadURL()
            .then((url) => {
              this.setState({ Url: url });
              console.log(url);
              data
                .child(this.state.id)
                .child("Url_document")
                .set(this.state.Url, (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log("url pushed");
                    this.setState({ success: true, loading_url: false });
                  }
                });
            });
        }
      );
    }
  }

  signout() {
    firebase
      .auth()
      .signOut()
      .then(this.props.history.push("/"))
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    const { classes } = this.props;
    const verificaion = this.state.doc_data.verify;
    return (
      <React.Fragment>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Navbar />
          </AppBar>
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <Toolbar />
            <div className={classes.drawerContainer}>
              <List>
                <Link
                  to={{
                    pathname: `/doctor/dashboard/${this.state.id}/appointments/`,
                  }}
                  style={{ textDecoration: "none" }}
                >
                  <ListItem button key="Appointments">
                    <ListItemIcon>
                      <MailIcon />
                    </ListItemIcon>
                    <ListItemText primary="Appointments" />
                    <ListItem />
                  </ListItem>
                </Link>

                <ListItem button key="Income">
                  <ListItemIcon>
                    <MailIcon />
                  </ListItemIcon>
                  <ListItemText primary="Income" />
                  <ListItem />
                </ListItem>

                {/* <ListItem button key="Appointments">
                  <ListItemIcon>
                    <MailIcon />
                  </ListItemIcon>
                  <ListItemText primary="Appointments" />
                  <ListItem />
                </ListItem> */}

                <ListItem button key="LogOut" onClick={this.signout.bind(this)}>
                  <ListItemIcon>
                    <MailIcon />
                  </ListItemIcon>
                  <ListItemText primary="Logout" />
                  <ListItem />
                </ListItem>
              </List>
            </div>
          </Drawer>
          <Toolbar />
          <main className={classes.content}>
            <Toolbar />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Avatar
                  alt="Remy Sharp"
                  src={this.state.doc_data.Url}
                  className={classes.large}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography gutterBottom variant="h5" component="h2">
                  {this.state.doc_data.Name}
                </Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Paper style={{ padding: "10px" }}>
                  <Typography>
                    {this.state.doc_data.spec1}
                    <br />
                    {this.state.doc_data.spec2}
                    <br />
                    {this.state.doc_data.Bio}
                  </Typography>
                  <Typography>Email: {this.state.doc_data.Email}</Typography>
                  {verificaion ? (
                    <Typography>You are verfied</Typography>
                  ) : (
                    <>
                      <Typography>
                        Please upload your documents here .. will get back to
                        you in 3 days
                      </Typography>
                      <br />

                      <div className={classes.root}>
                        <input
                          accept="image/*"
                          className={classes.input}
                          id="icon-button-file"
                          type="file"
                          onChange={this.GetImage.bind(this)}
                        />

                        <label htmlFor="icon-button-file">
                          <IconButton
                            color="primary"
                            aria-label="upload picture"
                            component="span"
                          >
                            <PhotoCamera />
                          </IconButton>
                        </label>
                        <label htmlFor="contained-button-file">
                          <Button
                            variant="contained"
                            color="primary"
                            component="span"
                            onClick={this.UploadTask.bind(this)}
                          >
                            Upload
                          </Button>
                        </label>
                        <label>
                          {this.state.loading_url && <CircularProgress />}
                        </label>
                        <label>{this.state.success && <CheckIcon />}</label>
                      </div>
                    </>
                  )}
                </Paper>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}></Grid>
              <Grid item xs={12}>
                <Link
                  to={{
                    pathname: `/doctor/update/form/${this.state.id}`,
                  }}
                  style={{ textDecoration: "none" }}
                >
                  <Button variant="contained" color="primary">
                    Update Profile
                  </Button>
                </Link>
              </Grid>
            </Grid>
          </main>
        </div>
        <Lines customLoading={this.state.loading} />
      </React.Fragment>
    );
  }
}

export default withStyles(DepartmentCardsStyles)(DoctorDashboard);
