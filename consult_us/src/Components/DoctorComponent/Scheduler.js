import React from "react";
import "rc-time-picker/assets/index.css";
import moment from "moment";
import TimePicker from "rc-time-picker";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { DoctorId } from "../../containers/getIdContainer/getId";

import {
  handleFlagMon,
  handleFlagTue,
  handleFlagWed,
  handleFlagThur,
  handleFlagFri,
  handleFlagSat,
  handleFlagSun,
} from "./temp_functions";

import firebase from "firebase";
// import firebaseDB from "../../containers/AuthContainer/vh_auth";

const DoctorDb = firebase.database().ref("Doctors");
const storage = firebase.storage().ref();

export default function Scheduler(props) {
  // for all the states for all days
  // create a disabled flag for disabling all checkboxes
  // var history = props.history
  // console.log(props.history);

  const [scheduleState,setScheduleState] = React.useState({
    schedule: [],
    error:"",
  });

  const [stateMon, setStateMon] = React.useState({
    // array:[],
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // for tuesday.....

  const [stateTue, setStateTues] = React.useState({
    // array:[],
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // Wednesday
  const [stateWed, setStateWed] = React.useState({
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // thursday
  const [stateThur, setStateThur] = React.useState({
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // Friday
  const [stateFri, setStateFri] = React.useState({
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // saturday
  const [stateSat, setStateSat] = React.useState({
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  // sunday
  const [stateSun, setStateSun] = React.useState({
    flag1: false,
    flag2: false,
    day: "",
    start_time_hr: "",
    start_time_min: "",
    end_time_hr: "",
    end_time_min: "",
    slots: "",
  });

  if (props.flag) {
    var data = {
      Name: props.data.Name,
      Email: props.data.Email,
      Url: props.data.Url,
      Specialization: {
        spec1: props.data.Specialization.spec1,
        spec2: props.data.Specialization.spec2,
      },
      Department: props.data.Department,
      Bio: props.data.Bio,
      Verification_status: 0,
    };

    firebase
      .auth()
      .createUserWithEmailAndPassword(props.data.Email, props.data.password)
      .then((user) => {
        // console.log("DoctorId", idState);
        DoctorDb.child(DoctorId)
          .set(data, (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("data pushed");
            }
          })
          .then(
            DoctorDb.child("Doctor_List")
              .child(DoctorId)
              .set(props.data.Name, (err) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log("data pushed to doctor list");
                }
              })
          )
          .then(function () {
            if (slotPush()) {
              updateID();
            }
          });
      })
      .catch((error) => {
        console.log(error);
        setScheduleState({
          error: "Invalid Email or password",
          Email: "",
          password: "",
        });
      });

    // history.push("/doctor/dashboard/" + `${DoctorId - 1}`);

    function slotPush() {
      console.log("inside");
      console.log(scheduleState.schedule);

      // ================== firebase code ================//

      scheduleState.schedule.forEach(function (item, index) {
        // if (item.day == "Monday") {
        console.log(item);
        var data = {
          start_time: item.start_time_hr + ":" + item.start_time_min,
          end_time: item.end_time_hr + ":" + item.end_time_min,
        };
        // console.log(idState);

        // firebase function...
        DoctorDb.child(DoctorId)
          .child("Schedule")
          .child(item.day)
          .set(data, (err) => {
            if (err) {
              // display error
            } else {
              console.log("start and end time pushed");
            }
          })
          .then(function () {
            for (var i = 0; i < item.slots; i++) {
              var slotid = "slot" + i.toString();
              console.log(DoctorId);
              DoctorDb.child(DoctorId-1)
                .child("Schedule")
                .child(item.day)
                .child("Slot")
                .child(slotid)
                .set(-1, (err) => {
                  if (err) {
                    // display error
                  } else {
                    // slot pushed
                    console.log("slot pushed");
                  }
                });
            }
          });
      });

      return true;
    }

    function updateID() {
      DoctorDb.child("ID").set(DoctorId + 1, (err) => {
        if (err) console.log(err);
        else {
          console.log("set id");
          props.history.push("/doctor/dashboard/" + `${DoctorId - 1}`);
        }
      });
    }
  }

  return (
    <>
      <Grid container>
        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagMon(val, stateMon, setStateMon, scheduleState);
                }}
                name="Monday"
              />
            }
            label="Monday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateMon.flag1}
            onChange={function (value) {
              if (value) {
                stateMon.start_time_hr = value.format("HH");
                stateMon.start_time_min = value.format("mm");
                setStateMon({
                  ...stateMon,
                  flag2: true,
                });
              } else {
                stateMon.start_time_hr = "";
                stateMon.start_time_min = "";
              }
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateMon.flag2}
            onChange={function (value) {
              stateMon.end_time_hr = value.format("HH");
              stateMon.end_time_min = value.format("mm");
              var calu = (stateMon.end_time_hr - stateMon.start_time_hr) * 60;
              var min = stateMon.end_time_min - stateMon.start_time_min;
              stateMon.slots = (parseInt(calu) + parseInt(min)) / 20;
            }}
            onClose={() => {
              scheduleState.schedule.push(stateMon);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Tuesday */}
        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagTue(val, stateTue, setStateTues, scheduleState);
                }}
                name="Tuesday"
              /> //disabled={flagState.flag3} />
            }
            label="Tuesday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateTue.flag1}
            onChange={function (value) {
              if (value) {
                stateTue.start_time_hr = value.format("HH");
                stateTue.start_time_min = value.format("mm");
                setStateTues({
                  ...stateTue,
                  flag2: true,
                });

                console.log(stateTue);
              } else {
                stateTue.start_time_hr = "";
                stateTue.start_time_min = "";
              }
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateTue.flag2}
            onChange={function (value) {
              stateTue.end_time_hr = value.format("HH");
              stateTue.end_time_min = value.format("mm");
              var calu = (stateTue.end_time_hr - stateTue.start_time_hr) * 60;
              var min = stateTue.end_time_min - stateTue.start_time_min;
              stateTue.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateTue);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateTue);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Wednesday */}

        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagWed(val, stateWed, setStateWed, scheduleState);
                }}
                name="Wednesday"
              /> //disabled={flagState.flag3} />
            }
            label="Wednesday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateWed.flag1}
            onChange={function (value) {
              stateWed.start_time_hr = value.format("HH");
              stateWed.start_time_min = value.format("mm");
              setStateWed({
                ...stateWed,
                flag2: true,
              });

              console.log(stateWed);
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateWed.flag2}
            onChange={function (value) {
              stateWed.end_time_hr = value.format("HH");
              stateWed.end_time_min = value.format("mm");
              var calu = (stateWed.end_time_hr - stateWed.start_time_hr) * 60;
              var min = stateWed.end_time_min - stateWed.start_time_min;
              stateWed.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateWed);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateWed);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Thrusday  */}

        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagThur(val, stateThur, setStateThur, scheduleState);
                }}
                name="Thursday"
              /> //disabled={flagState.flag3} />
            }
            label="Thursday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateThur.flag1}
            onChange={function (value) {
              stateThur.start_time_hr = value.format("HH");
              stateThur.start_time_min = value.format("mm");
              setStateThur({
                ...stateThur,
                flag2: true,
              });

              console.log(stateThur);
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateThur.flag2}
            onChange={function (value) {
              stateThur.end_time_hr = value.format("HH");
              stateThur.end_time_min = value.format("mm");
              var calu = (stateThur.end_time_hr - stateThur.start_time_hr) * 60;
              var min = stateThur.end_time_min - stateThur.start_time_min;
              stateThur.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateThur);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateThur);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Friday */}

        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagFri(val, stateFri, setStateFri, scheduleState);
                }}
                name="Friday"
              /> //disabled={flagState.flag3} />
            }
            label="Friday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateFri.flag1}
            onChange={function (value) {
              stateFri.start_time_hr = value.format("HH");
              stateFri.start_time_min = value.format("mm");
              setStateFri({
                ...stateFri,
                flag2: true,
              });

              console.log(stateFri);
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateFri.flag2}
            onChange={function (value) {
              stateFri.end_time_hr = value.format("HH");
              stateFri.end_time_min = value.format("mm");
              var calu = (stateFri.end_time_hr - stateFri.start_time_hr) * 60;
              var min = stateFri.end_time_min - stateFri.start_time_min;
              stateFri.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateFri);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateFri);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Saturday */}

        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagSat(val, stateSat, setStateSat, scheduleState);
                }}
                name="Saturday"
              /> //disabled={flagState.flag3} />
            }
            label="Saturday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateSat.flag1}
            onChange={function (value) {
              stateSat.start_time_hr = value.format("HH");
              stateSat.start_time_min = value.format("mm");
              setStateSat({
                ...stateSat,
                flag2: true,
              });

              console.log(stateSat);
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateSat.flag2}
            onChange={function (value) {
              stateSat.end_time_hr = value.format("HH");
              stateSat.end_time_min = value.format("mm");
              var calu = (stateSat.end_time_hr - stateSat.start_time_hr) * 60;
              var min = stateSat.end_time_min - stateSat.start_time_min;
              stateSat.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateSat);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateSat);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>

        {/* Sunday */}

        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={function (evt) {
                  const val = evt.target.checked;
                  handleFlagSun(val, stateSun, setStateSun, scheduleState);
                }}
                name="Sunday"
              /> //disabled={flagState.flag3} />
            }
            label="Sunday"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px" }}
            minuteStep={20}
            disabled={!stateSun.flag1}
            onChange={function (value) {
              stateSun.start_time_hr = value.format("HH");
              stateSun.start_time_min = value.format("mm");
              setStateSun({
                ...stateSun,
                flag2: true,
              });

              console.log(stateSun);
            }}
            id="mon-st"
            name="start_time"
          />
        </Grid>
        <Grid item xs={4}>
          <TimePicker
            placeholder="15:00"
            showSecond={false}
            style={{ marginTop: "5px", marginLeft: "0.5rem" }}
            minuteStep={20}
            disabled={!stateSun.flag2}
            onChange={function (value) {
              stateSun.end_time_hr = value.format("HH");
              stateSun.end_time_min = value.format("mm");
              var calu = (stateSun.end_time_hr - stateSun.start_time_hr) * 60;
              var min = stateSun.end_time_min - stateSun.start_time_min;
              stateSun.slots = (parseInt(calu) + parseInt(min)) / 20;

              console.log(stateSun);
            }}
            onClose={() => {
              scheduleState.schedule.push(stateSun);
              console.log(scheduleState.schedule);
            }}
            id="mon-et"
            name="end_time"
          />
        </Grid>
      </Grid>

      {/* <Button onClick={checkClick}>Trial Button</Button> */}
    </>
  );
}

/*
state1
{
  day:tues
  start-hr: 
  end-hr :
}
state2
[].push(state_mon)
[{day:monday,starthr:},{day:tues,stathr:end}]

for dict in array 
if(dict.day == Monday )

else if 


*/

/*

 if (props.flag) {
    var data = {
      Name: props.data.Name,
      Email: props.data.Email,
      Url: props.data.Url,
      Specialization: {
        spec1: props.data.Specialization.spec1,
        spec2: props.data.Specialization.spec2,
      },
      Department: props.data.Department,
      Bio: props.data.Bio,
      Verification_status: 0,
    };

    console.log("sch data", data, DoctorId);



      <Grid item xs={12}>
          <Typography style={{ textAlign: "center" }}>Schedule</Typography>
        </Grid>

        <Grid item xs={4}>
          <FormLabel component="legend">Day</FormLabel>
        </Grid>
        <Grid item xs={4}>
          <FormLabel component="legend">Start Time</FormLabel>
        </Grid>
        <Grid item xs={4}>
          <FormLabel component="legend">End Time</FormLabel>
        </Grid>

 */
