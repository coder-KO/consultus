import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../../containers/GeneralCss/UseStyles";
import firebase from "firebase";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { Footer, Navbar } from "../Layouts";

const DoctorDb = firebase.database().ref("Doctors");

export default function DoctorLoginForm(props) {
  //   console.log(props.match.params.docid);
  const classes = useStyles();
  const [state, setState] = React.useState({
    id: "",
    Email: "",
  });

  // calling the function to load data...

  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  }

  // Form Submission

  function handleSubmit(event) {
    console.log("in submission");

    event.preventDefault();
    console.log(state.Email)

    DoctorDb.once("value").then((snapshot) => {
      console.log(snapshot.val())
      snapshot.forEach((childSnapshot) => {
        const values = childSnapshot.val();
        console.log(values.Email===state.Email)
    
        if (values.Email === state.Email) {
            // console.log("user exists");
            firebase.auth().sendPasswordResetEmail(state.Email).then(function() {
              console.log("sent")
                return(
                    <Typography>
                       You will receive the Password Reset details on your mail
                    </Typography>
                )
              }).catch(function(error) {
                  console.log("user not registered")
              });
              
        }
      });
    });
  }

  return (
    <>
      <Navbar />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.purple}>
            <AssignmentIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Password Reset{" "}
          </Typography>
          <ValidatorForm className={classes.form} onSubmit={handleSubmit}>
            {/* validation field for email  */}

            <TextValidator
              margin="normal"
              fullWidth
              label="Please Enter your registerd Email"
              onChange={handleChange}
              name="Email"
              value={state.Email}
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              // onClick={handleSubmit}
            >
              Submit{" "}
            </Button>
          </ValidatorForm>
        </div>
        <Footer />
      </Container>
    </>
  );
}
