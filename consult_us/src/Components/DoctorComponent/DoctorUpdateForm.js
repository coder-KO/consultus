import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../../containers/GeneralCss/UseStyles";
import firebase from "firebase";
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { Footer, Navbar } from "../Layouts";
import { Redirect } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from "@material-ui/icons/Check";

const DoctorDb = firebase.database().ref("Doctors");
const storage = firebase.storage().ref();

export default function DoctorUpdateForm(props) {
  //   console.log(props.match.params.docid);

  // firebase auth call setup

  React.useEffect(() => {
    console.log("hello");
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.
        console.log(user.email);
        // this.setState({ id: this.props.match.params.docid });
        console.log(props.match.params.docid);
        DoctorDb.child(`${props.match.params.docid}`)
          .once("value")
          .then((snapshot) => {
            const data_rec = snapshot.val();
            console.log(data_rec);
            if (data_rec.Email !== user.email) {
              props.history.push("/doctor/login/form");
            }
          });
      } else {
        // No user is signed in.
        props.history.push("/doctor/login/form");
      }
    });
  });

  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  const [state2, setState2] = React.useState({
    spec1: "",
    spec2: "",
  });

  const [state1, setState1] = React.useState({
    image: "",
    Url: "",
    flag: 0,
  });

  // calling the function to load data...

  function GetImage(e) {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      state1.image = image;
    }
    state1.flag = 1;
    console.log(e.target.files[0], state1.flag);
  }

  function UploadTask(e) {
    if (!loading) {
      const image = state1.image;
      setSuccess(false);
      setLoading(true);

      if (state1.image === "") {
        setSuccess(true);
        setLoading(false);
        return null;
      }
      const upload = storage
        .child("Doctors")
        .child(props.match.params.docid + `/${props.match.params.docid}`)
        .put(image);
      console.log("this is upload_task", upload);
      upload.on(
        "state_changed",
        (snapshot) => {
          // progress function ...
          // const progress = Math.round(
          //   (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          // );
        },
        (error) => {
          // Error function ...
          console.log(error);
        },
        () => {
          // complete function ...

          storage
            .child("Doctors")
            .child(props.match.params.docid + "/" + props.match.params.docid)
            .getDownloadURL()
            .then((url) => {
              state1.Url = url;
              // state1.flag = 0;
              setSuccess(true);
              setLoading(false);
              console.log(state1);
            });
        }
      );
    }
  }

  function handleChange(evt) {
    const value = evt.target.value;
    setState2({
      ...state2,
      [evt.target.name]: value,
    });
  }

  // Form Submission

  function handleSubmit(event) {
    console.log("in submission");

    event.preventDefault();
    // console.log(state, state1);

    if (state2.spec1 !== "") {
      // state2.spec1 = state.spec1;

      DoctorDb.child(props.match.params.docid)
        .child("Specialization")
        .child("spec1")
        .set(state2.spec1, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("data updated");
          }
        });
    }

    if (state2.spec2 !== "") {
      // state2.spec2 = state.spec2;
      DoctorDb.child(props.match.params.docid)
        .child("Specialization")
        .child("spec2")
        .set(state2.spec2, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("data updated");
          }
        });
    }

    if (state1.Url !== "") {
      DoctorDb.child(props.match.params.docid)
        .child("Url")
        .set(state1.Url, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("data updated");
          }
        });
    }

    // eslint-disable-next-line no-useless-concat
    props.history.push("/doctor/dashboard/" + `${props.match.params.docid}`);
  }

  return (
    <>
      <Navbar />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.purple}>
            <AssignmentIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Data Update Form
          </Typography>
          <ValidatorForm className={classes.form} onSubmit={handleSubmit}>
            {/* Specialisation for doctor */}

            <TextValidator
              margin="normal"
              fullWidth
              label="Specialisation 1"
              onChange={handleChange}
              name="spec1"
              value={state2.spec1}
            />

            <TextValidator
              margin="normal"
              fullWidth
              label="Specialisation 2"
              onChange={handleChange}
              name="spec2"
              value={state2.spec2}
            />

            <br />
            <br />

            {/* uploading image  */}

            <FormLabel>Change Profile picture</FormLabel>

            <div className={classes.root}>
              <input
                accept="image/*"
                className={classes.input}
                id="icon-button-file"
                type="file"
                onChange={GetImage}
              />

              <label htmlFor="icon-button-file">
                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                >
                  <PhotoCamera />
                </IconButton>
              </label>
              <label htmlFor="contained-button-file">
                <Button
                  variant="contained"
                  color="primary"
                  component="span"
                  onClick={UploadTask}
                >
                  Upload
                </Button>
              </label>
              <label>{loading && <CircularProgress />}</label>
              <label>{success && <CheckIcon />}</label>
            </div>

            <br />
            <br />

            {/* Checkbox */}
            <FormControlLabel
              control={
                <Checkbox value="remember" color="primary" required="true" />
              }
              label="I agree all the information given by me are correct"
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={loading}
            >
              Submit{" "}
            </Button>
          </ValidatorForm>
        </div>
        <Footer />
      </Container>
    </>
  );
}
