import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Lines } from "react-preloaders";

// import DepartmentCardsStyles from '../Departments/DepartmentCardsStyles';
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { withStyles } from "@material-ui/core/styles";

const DepartmentCardsStyles = (theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
});

const cards = [1, 2, 3, 4, 5, 6];

const data = firebaseDB.database().ref("Departments");
const storage = firebaseDB.storage().ref();

export class DepartmentCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departments: [],
      url: "",
      image: {},
      loading: true,
    };
  }

  componentDidMount() {
    var today = new Date()
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    console.log("date is ",date)
    data.on("value", (snapshot) => {
      let depts = snapshot.val();
      console.log(depts);
      let new_dept = [];
      for (let dept in depts) {
        console.log("here", dept + "/" + dept + ".jpeg");
        storage
          .child("Departments")
          .child(`${dept}` + "/" + `${dept}` + ".jpeg")
          .getDownloadURL()
          .then((url) => {
            console.log(url);
            this.state.image[dept] = url;
            new_dept.push({
              dept_name: dept,
              dept_description: depts[dept],
            });
          })
          .then(() => {
            this.setState({
              departments: new_dept,
              loading: false,
            });
          });
      }
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {this.state.departments.map((dept) => (
              <Grid item key={dept} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={this.state.image[dept.dept_name]}
                    title="Title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {dept.dept_name}
                    </Typography>
                    <Typography>{dept.dept_description}</Typography>
                  </CardContent>
                  <CardActions>
                    <Link
                      to={{
                        pathname: `/departments/doctors/${dept.dept_name}`,
                      }}
                      style={{ textDecoration: "none" }}
                    >
                      <Button>See Doctors</Button>
                    </Link>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
        <Lines customLoading={this.state.loading} />
        {/* // animation="slide" background="linear-gradient(180deg, #f95759 0%, #a62022 100%)"  /> */}
      </React.Fragment>
    );
  }
}

export default withStyles(DepartmentCardsStyles)(DepartmentCards);
