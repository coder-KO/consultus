import { withStyles } from '@material-ui/core/styles';

const DepartmentCardsStyles = theme =>({
    cardGrid: {
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(8),
      },
      card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      },
      cardMedia: {
        paddingTop: '56.25%', // 16:9
      },
      cardContent: {
        flexGrow: 1,
      },
});

export default withStyles(DepartmentCardsStyles);