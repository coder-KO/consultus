import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { Link } from "react-router-dom";
import { Lines } from "react-preloaders";

// import DepartmentCardsStyles from '../Departments/DepartmentCardsStyles';
import firebaseDB from "../../containers/AuthContainer/vh_auth";
import { withStyles } from "@material-ui/core/styles";
import { DoctorId } from "../../containers/getIdContainer/getId";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { Redirect } from "react-router-dom";

const DepartmentCardsStyles = (theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },

  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
});

const cards = [1, 2, 3, 4, 5, 6];

const data = firebaseDB.database().ref("Doctors");

export class DepartmentDoctor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [],
      total: "",
      dept: "",
      loading: true,
    };
  }

  componentDidMount() {
    const dept = this.props.match.params.name;
    // console.log(dept);
    this.setState({ dept: dept });
    data.once("value").then((snapshot) => {
      const Doc_arr = [];
      snapshot.forEach((childSnapshot) => {
        const values = childSnapshot.val();
        // console.log(values)
        if (
          values.Department === this.state.dept &&
          values.Verification_status === 1
        ) {
          const data = {
            Doc_name: values.Name,
            Doc_Spec1: values.Specialization.spec1,
            Doc_Spec2: values.Specialization.spec2,
            Doc_id: childSnapshot.key,
            Doc_url: values.Url,
            Doc_bio: values.Bio,
          };
          // console.log(data);
          Doc_arr.push(data);
        }
      });

      // console.log(Doc_arr);
      if (Doc_arr.length === 0) {
        return this.props.history.push("/error");
      }

      this.setState({
        doctors: Doc_arr,
        loading: false,
      });
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {this.state.doctors.map((doc) => (
              <Grid item key={doc} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={doc.Doc_url}
                    title="Title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {doc.Doc_name}
                    </Typography>
                    <Typography>{doc.Doc_bio}</Typography>
                    <Typography>
                      Specialization:
                      <br />
                      {doc.Doc_Spec1}
                      <br />
                      {doc.Doc_Spec2}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Link
                      to={{
                        pathname: `/patient/form/${this.state.dept}/${doc.Doc_id}/${doc.Doc_name}`,
                      }}
                      style={{ textDecoration: "none" }}
                    >
                      <Button>Book Appointment</Button>
                    </Link>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
        <Lines customLoading={this.state.loading} />
      </React.Fragment>
    );
  }
}

export default withStyles(DepartmentCardsStyles)(DepartmentDoctor);
