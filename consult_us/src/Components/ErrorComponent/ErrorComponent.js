import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function SimpleAlerts() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Alert variant="filled" severity="error">
        No Doctors avaialble right now..
      </Alert>

      <Typography className={classes.root}>
        <Link
          to={{
            pathname: `/home`,
          }}
        > click here to go back to home page </Link>
      </Typography>
    </div>
  );
}
