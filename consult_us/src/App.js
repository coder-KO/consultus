import React from "react";
import { Lines } from "react-preloaders";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./Components/HomeComponent/Homepage";
import DepartmentDoctor from "./Components/Departments/DepartmentDoctorComponent";
import PatientForm from "./Components/PatientComponent/PatientForm";
import DoctorUpdateForm from "./Components/DoctorComponent/DoctorUpdateForm";
import DoctorLoginForm from "./Components/DoctorComponent/DoctorLoginForm";
import DoctorDashboard from "./Components/DoctorComponent/DoctorDashboard";
import SendPasswordReset from "./Components/DoctorComponent/SendPasswordReset";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          {/* cards of doctors of particular departments */}
          <Route
            exact
            path="/departments/doctors/:name"
            component={DepartmentDoctor}
          />

          {/* Patient form integrated with backend */}
          <Route
            path="/patient/form/:department/:docid/:name"
            component={PatientForm}
          />

          {/* Adding doctor dashboard */}

          <Route path="/doctor/dashboard/:docid" component={DoctorDashboard} />

          {/* Doctor login form  */}

          <Route exact path="/doctor/login/form/" component={DoctorLoginForm} />

          {/* Doctor's Detail  Update form  */}

          <Route
            exact
            path="/doctor/update/form/:docid"
            component={DoctorUpdateForm}
          />
          {/* Password Reset Form  */}
          <Route exact path="/forgot/password/" component={SendPasswordReset} />

          {/* Homepage */}
          <Route path="/" component={HomePage} />
        </Switch>
      </div>
    </Router>
  );
}
